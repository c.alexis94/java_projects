/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercice5collection;

/**
 *
 * @author alexis
 */
public class Eleve 
{ 
    private String nom;
    private float moyenne;
    
    public Eleve (String unNom, float uneMoyenne)
    {
        this.nom = unNom;
        this.moyenne = uneMoyenne;
    }
    
    public String getNom()
    {
        return nom;
    }
    
    public float getMoyenne()
    {
        return moyenne;
    }
}
