/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercice5collection;
import java.util.ArrayList;
import java.util.HashMap; 
/**
 *
 * @author alexis
 */
public class Exercice5collection 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Eleve e1 = new Eleve("Toto", 10.5f);
        Eleve e2 = new Eleve("Tutu", 18f);
        Eleve e3 = new Eleve("Titi", 8.8f);
        Eleve e4 = new Eleve("TomPouce", 5.5f);
        
        Unite u = new Unite();
        
        u.ajoutEleve(e1);
        u.ajoutEleve(e2);
        u.ajoutEleve(e3);
        u.ajoutEleve(e4);
        
        Eleve eMeilleur = u.getEleveMeilleur();
        System.out.println("Le meilleur : "+ eMeilleur.getNom());
        
        Eleve eRecherche = u.getEleve("Titi");
        System.out.println("La recherche : "+ eRecherche.getNom());
        
        System.out.println("Les eleves n'ayant pas la moyenne : ");
        ArrayList<Eleve> ListeInf10 = u.getEleveMoyInf10();
        for (Eleve e : ListeInf10)
        {
            System.out.println(e.getNom());
        }
        
        
        
        // TODO code application logic here
    }
    
}
