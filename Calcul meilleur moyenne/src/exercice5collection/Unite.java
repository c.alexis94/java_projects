/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercice5collection;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author alexis
 */
public class Unite 
{
    HashMap<String, Eleve> lesEleves = new HashMap<String, Eleve>();
    public Unite()
    {
        this.lesEleves = new HashMap<String, Eleve>();
    }
    
    public void ajoutEleve(Eleve e)
    {
        this.lesEleves.put(e.getNom(),e);
    }
    
    public Eleve getEleveMeilleur()
    {
        Eleve retour = null;
        float moy = 0f;
        for (Eleve e : this.lesEleves.values())
        {
            if (e.getMoyenne()> moy)
            {
                moy = e.getMoyenne();
                retour = e;
            }
        }
        return(retour);
    }
    
    public Eleve getEleve(String nom)
    {
        Eleve retour = null;
        retour = this.lesEleves.get(nom);
        return (retour);
    }
    
    public ArrayList<Eleve> getEleveMoyInf10()
    {
        ArrayList<Eleve> LesInf10 = new ArrayList<Eleve>();
        for (Eleve e : this.lesEleves.values())
        {
            if (e.getMoyenne()<10)
            {
                LesInf10.add(e);
            }
        }
        return(LesInf10);
    }
}
