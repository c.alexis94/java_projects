/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heritagejava;

/**
 *
 * @author alexis
 */
public class CompteEpargne extends Compte  
{
    private float taux;
    public CompteEpargne(int numero, float solde, String libelle)
    {
        super(numero, solde, libelle);//appel constructeur de compte
        this.taux = 0.05f;
    }
    
    public float interets()
    {
        return this.taux * this.solde;//solde est défini en protected
    }
    
    public String toString()
    {
        String retour;
        retour = super.toString() +" Taux : "+ this.taux;
        retour += "interets : " + this.interets();
        return retour;
    }
}
