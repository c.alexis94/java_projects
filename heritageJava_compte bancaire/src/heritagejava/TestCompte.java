/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heritagejava;

/**
 *
 * @author alexis
 */
public class TestCompte 
{
    public static void main(String args[])
    {
       Compte cl = new Compte (1, 200.0f, "Compte Epargne 1 ");
       if (cl.debiter(500) == false)
       {
           System.out.println("Débit impossible");
       }
       cl.crediter(10);
       System.out.println(cl.toString());
    }
}
