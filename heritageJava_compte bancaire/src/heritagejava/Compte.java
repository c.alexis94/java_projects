/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heritagejava;

/**
 *
 * @author alexis
 */
public class Compte 
{
    private int numero;
    protected float solde;
    private String libelle;
    private static int nbCompte = 0;
    
    public Compte (int numero, float solde, String libelle)
    {
        this.numero = numero;
        this.solde = solde;
        this.libelle = libelle;
        Compte.nbCompte++;
    }
    
    public boolean debiter (float mnt)
    {
        boolean resultat = false;
        if (mnt <= this.solde)
        {
            this.solde = this.solde - mnt;
            resultat = true;
        }
        
        return resultat;
    }
    
    public void crediter (float mnt)
    {
         this.solde += mnt;       
    }
    
    public String toString ()
    {
        String s;
    s = "Compte n° : "+ this.numero + " solde : " + this.solde;
        return s;
    }
    
    public static int getNbCompte()
    {
        return Compte.nbCompte;
    }
}

