/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapitre6;

/**
 *
 * @author alexis
 */
class Chat extends Animal 
{
    public Chat(String unNom, int unPoid)
    {     
        super(unNom, unPoid);
    }
    
    public void mange()
    {
        System.out.println("Je mange des souris");
    }
}
