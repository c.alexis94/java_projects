/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapitre6;

/**
 *
 * @author alexis
 */
abstract class Animal
{
    private String nom;
    private int poid;

    public Animal (String unNom, int unPoid)  
    {
        this.nom= unNom;
        this.poid = unPoid;
    }

    public void affiche () 
    {
        System.out.println("Je suis "+ this.nom);
        System.out.println("Je pèse "+ this.poid +" kg");
    }
    abstract void mange();
}