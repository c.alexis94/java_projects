/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapitre6;

/**
 *
 * @author alexis
 */
public class Chapitre6 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
    
    Chat c = new Chat("minet", 3);
    c.affiche();
    c.mange(); 
    
    Elephant e = new Elephant("Jumbo", 3000);
    e.affiche(); // appel de la méthode de la classe Animal
    e.mange();
    }
}
