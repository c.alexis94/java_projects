/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testlivre;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author alexis
 */
public class testLivre 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        ArrayList <Livre> col = new ArrayList<Livre>();
        for (int i=0 ; i<3 ; i++)
        {
            Livre l1 = new Livre();
            l1.saisie();
            col.add(l1); // ajout de livre dans la collection
        }
        for(Livre unLivre : col)
        {
            System.out.println(unLivre);
        }
        // suppression du 1ier élément (la méthode décale les données)
        col.remove(0);
        // récupération du 1ier élément
        Livre l2 = col.get(0);
        System.out.println("Après suppression, le 1er élément est : "+l2);
        
        Livre l3= new Livre();
        l3.saisie();
        if (col.contains(l3))
        {
            System.out.println("Le livre est en index : "+col.indexOf(l3));
        } // indexOf renvoit -1 si non trouvé
        else 
        {
            System.out.println("la collection ne contient pas "+l3);
        }
        
        Collections.sort(col);
        for(Livre livre : col) 
        { 
            System.out.println(livre); 
        }       
    }   
}
