/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testlivre;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author alexis
 */
public class Livre implements Comparable<Livre>
{
    String titre;
    String auteur;
    
    public String toString()
    {
        return "Titre : "+this.titre+" auteur : "+this.auteur;
    }
    
    public boolean equals(Object o)
    { // méthode redéfinie
        String t =((Livre) o).titre;
        String a =((Livre) o).auteur;
        return (this.titre.equals(t) && this.auteur.equals(a));
    }
    
    public void saisie()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir le titre du livre : ");
        this.titre = sc.next();
        System.out.println("Saisir l'auteur du livre : ");
        this.auteur = sc.next();
    }
    
    public int compareTo(Livre l)// méthode de l’interface 
    {
        return(this.titre.compareTo(l.titre));
    }
}
