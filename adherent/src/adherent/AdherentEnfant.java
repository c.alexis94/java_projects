/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adherent;

/**
 *
 * @author alexis
 */
public class AdherentEnfant extends Adherent 
{
    private int age;
    public AdherentEnfant(String pNom, String pVille, int pAge)
    {
        super(pNom, pVille);
        this.age = pAge;
    }
    
    public boolean gratuit()
    {
        boolean ret;
        if (this.age < 12 )
        {
            ret = true;   
        }
        else
        {
            ret = false;
        }
        return (ret);
    }   
}
