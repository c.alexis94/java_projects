/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package adherent;

import java.util.ArrayList;
/**
 *
 * @author alexis
 */
public class EnsembleAdherents 
{
    private ArrayList<Adherent> lesAdherents;
    public EnsembleAdherents()
    {
        this.lesAdherents = new ArrayList<Adherent>();
    }
    
    public void ajouterUnAdherent (Adherent unAdherent)
    {
        this.lesAdherents.add(unAdherent);
    }
    
    public ArrayList<Adherent> getLesAdherents()
    {
        return this.lesAdherents;
    }
    
    public ArrayList<Adherent> getLesGratuits()
    {
        ArrayList<Adherent> ret = new ArrayList<Adherent>();
        for (Adherent a : this.lesAdherents)
        {
            if (a.gratuit() == true)
            {
                ret.add(a);
            }
        }return ret;
    }
    
}
