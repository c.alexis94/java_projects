/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adherent;
import java.util.ArrayList;
/**
 *
 * @author alexis
 */
public class Adherent {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Adherent a1 = new Adherent ("toto", "Paris");
        Adherent a2 = new Adherent ("titi", "Marseille");
        AdherentEnfant a3 = new AdherentEnfant("jojo", "Goyave", 13);
        AdherentEnfant a4 = new AdherentEnfant("jaja", "Lyon", 10);
        EnsembleAdherents ens = new EnsembleAdherents();
        ens.ajouterUnAdherent(a1);
        ens.ajouterUnAdherent(a2);
        ens.ajouterUnAdherent(a3);
        ens.ajouterUnAdherent(a4);
        ArrayList<Adherent> retour = ens.getLesGratuits();
        System.out.println("Liste des adhérents gratuits");
        for (Adherent a : retour)
        {
            System.out.println(a);
        }
        // TODO code application logic here
    }
    
    private String nom;
    private String ville;
    
    public Adherent(String pNom, String pVille)
    {
        this.nom = pNom;
        this.ville = pVille;
    }
    public String toString()
    {
        String s;
        s = "Nom : " + this.nom + " Ville : " + this.ville;
        return s;
    }
    public boolean gratuit()
    {   
       boolean ret;
       if (this.ville == "Paris")
       {
          ret = true;
       }
       else
       {
          ret = false;
       }
       return (ret);
    }
}
